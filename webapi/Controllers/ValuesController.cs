﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Services;

namespace webapi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ValuesController : ControllerBase
    {
        private Service service;
        public ValuesController(Service service) {
            this.service = service;
        }
        // GET api/values
        [HttpGet]
        public ActionResult<IEnumerable<string>> Get()
        {
            return new string[] { service.GetTrueForTest().ToString() };
        }

        // GET api/values/5
        [HttpGet("{input}")]
        public async Task<ActionResult<int>> Get(int input)
        {
            return await service.DoWork(input);
        }

        // POST api/values
        [HttpPost]
        public void Post([FromBody] string value)
        {
        }
    }
}
