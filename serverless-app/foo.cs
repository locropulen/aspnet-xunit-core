using System;
using System.IO;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Azure.WebJobs;
using Microsoft.Azure.WebJobs.Extensions.Http;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using Services;

namespace serverless_app
{
    public static class foo
    {
        [FunctionName("foo")]
        public static async Task<IActionResult> Run([HttpTrigger(AuthorizationLevel.Function, "get", "post", Route = null)] HttpRequest req,
            ILogger log)
        {
            var service = new Service();

            log.LogInformation("C# HTTP trigger function processed a request.");
            var response = new string[] { service.GetTrueForTest().ToString() };

            return (ActionResult)new OkObjectResult(response);

        }

    }
}