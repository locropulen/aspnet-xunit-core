using System;
using Xunit;
using System.Linq;
using System.Collections.Generic;
using System.ComponentModel;

namespace xunit_test_files
{
    public class AnonymousTypesTest
    {
        [Fact]
        public void ShouldFindDistinctInArrayUsingLambdas()
        {
            var product1 = new { Name = "Product 1", Benefits = new[] { "0", "1", "2" } };
            var product2 = new { Name = "Product 1", Benefits = new[] { "2", "3", "4" } };
            var products = new[] { product1, product2 };
            var foundBenefits = products.Select(x => x.Benefits);

            var allBenefits = foundBenefits.Aggregate((x1, x2) => x1.Concat(x2).ToArray()).Distinct();

            Console.WriteLine(string.Join(",", allBenefits));

            Assert.Equal(5, allBenefits.Count());
        }
    }
}