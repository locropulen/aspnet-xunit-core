using System;
using System.Linq;
using Xunit;
using System.IO;
using System.Reflection;
using Newtonsoft.Json;

namespace xunit_test_files
{
    public class JSONStructureTest
    {
        [Fact]
        public void ShouldLoadJSONFileAsObjectAsInNode()
        {
            var parentDirectory = Directory.GetParent(Directory.GetCurrentDirectory()).Parent.Parent.FullName;

            // parentDirectory concats FooFolder
            string directoryPath = String.Format("{0}/FooFolder", parentDirectory);

            dynamic product;
            using (StreamReader r = new StreamReader(string.Format("{0}/FooJson.json", directoryPath)))
            {
                string json = r.ReadToEnd();
                product = JsonConvert.DeserializeObject(json);
            }

            Assert.Equal(product.Name.ToString(), "product name");
            Assert.Equal(product.Benefits.Count, 3);
        }
    }
}