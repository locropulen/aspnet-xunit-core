using System;
using System.Linq;
using Xunit;
using System.IO;
using System.Reflection;

namespace xunit_test_files
{
    public class FileSystemTest
    {
        [Fact]
        public void ShouldSomeFilesBeFound()
        {
            string someFile = String.Format("{0}/tests.pdb", Directory.GetCurrentDirectory());
            // Console.WriteLine(someFile);
            Assert.True(File.Exists(someFile));
        }
        [Fact(Skip = "only on local")]
        public void ShouldFilesInTheSpecifFolderBeFound()
        {
            var directory = new DirectoryInfo("/home/zetta/xunit-test-files/FooFolder/");

            Assert.Equal(2, directory.GetFiles().Length);
        }

        [Fact]
        public void ShouldFilesInTheProperFolderBeFound()
        {
            // Console.WriteLine(Directory.GetParent(Directory.GetCurrentDirectory()).Parent.FullName);
            // Console.WriteLine(Assembly.GetExecutingAssembly().CodeBase);

            // go back to the root from bin/Debug/netcoreapp2.2/ subdirectory
            var parentDirectory = Directory.GetParent(Directory.GetCurrentDirectory()).Parent.Parent.FullName;

            // parentDirectory concats FooFolder
            string directoryPath = String.Format("{0}/FooFolder", parentDirectory);

            var directory = new DirectoryInfo(directoryPath);

            var myfile = from a in directory.GetFiles() where a.Name == "FileFoo3.txt" select a.Name;

            Assert.Equal(2, directory.GetFiles().Length);
            Assert.Equal(myfile.Count(), 1);
        }
    }
}