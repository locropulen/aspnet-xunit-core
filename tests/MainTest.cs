using System;
using Xunit;
using Services;

namespace dotnet
{
  public class MainTest
  {
    internal static Service service;
    public MainTest()
    {
      MainTest.service = new Services.Service();
    }

    [Fact]
    public void ShouldDependencyInjectionWork()
    {
      Assert.True(MainTest.service.GetTrueForTest());
    }

    [Fact]
    public void ShouldTryDelegate()
    {
      Assert.Equal(1, MainTest.service.SearchInList(x => x > 2).Count);
    }
  }
}
