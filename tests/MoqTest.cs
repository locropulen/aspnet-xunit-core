using System;
using Xunit;
using Services;
using Moq;

namespace dotnet
{
    public class MoqTest
    {
        public MoqTest() { }

        [Fact]
        public void ShouldMoqWork()
        {
            var mock = new Mock<Service>();

            mock.Setup(m => m.GetTrueForTest()).Returns(false);

            var mockObject = mock.Object;

            Assert.False(mockObject.GetTrueForTest());
        }
    }
}







