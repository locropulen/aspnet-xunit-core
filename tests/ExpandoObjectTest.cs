using System;
using System.Dynamic;
using Xunit;
using System.Collections.Generic;
using System.ComponentModel;

namespace xunit_test_files
{
    public class ExpandoObjectTest
    {
        [Fact]
        public void ShouldPropertyBeRemoved()
        {
            dynamic sampleObject = new ExpandoObject();
            sampleObject.Message = "Hello";
            Assert.True(((IDictionary<String, Object>)sampleObject).ContainsKey("Message"));
        }
    }
}