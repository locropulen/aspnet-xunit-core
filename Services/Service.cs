using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Services
{
    public class Service

    {
        public virtual bool GetTrueForTest()
        {
            return true;
        }

        public Task<int> DoWork(int input)
        {
            Func<int> function = new Func<int>(() => GetSum(input, 5));
            return Task.Run<int>(function);
        }

        public List<int> SearchInList(Predicate<int> condition)
        {
            List<int> list = new List<int> { 1, 2, 3 };

            List<int> newList = list.FindAll(condition);

            return newList;
        }

        private int GetSum(int a, int b)
        {
            return a + b;
        }

    }
}