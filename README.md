# aspnet-xunit-core

Understanding and reviewing some basic concepts about netcoreapp2.2 with C#

Done in ubuntu, vscode, and vscode-solution-explorer

https://github.com/fernandoescolar/vscode-solution-explorer

https://blog.ploeh.dk/

https://github.com/OmniSharp/omnisharp-vscode

## Getting Started

See `tasks.json` in .vscode

https://bitbucket.org/locropulen/aspnet-xunit-core/src/master/.vscode/tasks.json

### serverless-app

Microsoft.NET.Sdk.Functions

Http Functions running on localhost using `ms-azuretools.vscode-azurefunctions` vscode extension

#### Why is it like that :/

https://github.com/Azure/azure-functions-host/issues/3736

```
foo: [GET,POST] http://localhost:7071/api/foo

values: [GET,POST] http://localhost:7071/api/values
```

### WebApi + cors

ApiControllers / AspNetCore.App

Microsoft.AspNetCore.App

Microsoft.AspNetCore.Cors

ProjectReference `Services`

```
Hosting environment: Development
Content root path: /aspnet-xunit-core/webapi
Now listening on: https://localhost:5001
Now listening on: http://localhost:5000
Application started. Press Ctrl+C to shut down.
```

### Services

netstandard2.0

Playing with Func<> Predicate<> and Task<> 

https://bitbucket.org/locropulen/aspnet-xunit-core/src/master/Services/Service.cs

### Unit Tests

Microsoft.NET.Test.Sdk

Playing with JSONs, Anonymous Methods, Lambda Expresions, dynamics

https://xunit.net/

https://www.nuget.org/packages/moq/

ProjectReference `Services`

https://bitbucket.org/locropulen/aspnet-xunit-core/src/master/tests/MoqTest.cs

See results

https://bitbucket.org/locropulen/aspnet-xunit-core/addon/pipelines/home#!/results/3
### C# Scripting looks cool

Not much just trying `Console.WriteLine` stuffs

```
dotnet tool install -g dotnet-script
```

https://www.hanselman.com/blog/CAndNETCoreScriptingWithTheDotnetscriptGlobalTool.aspx

https://github.com/filipw/dotnet-script?WT.mc_id=-blog-scottha

## License

Unlincesed